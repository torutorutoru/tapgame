//
//  KDAppDelegate.h
//  TapGame
//
//  Created by Toru Hisai on 13/01/14.
//  Copyright (c) 2013年 Kronecker's Delta Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property int stackRef;
@end
