//
//  KDAppDelegate.m
//  TapGame
//
//  Created by Toru Hisai on 13/01/14.
//  Copyright (c) 2013年 Kronecker's Delta Studio. All rights reserved.
//

#import "KDAppDelegate.h"
#import "lua.h"
#import "lualib.h"
#import "lauxlib.h"
#import "LuaBridge.h"

int luaopen_cg(lua_State*);

@implementation KDAppDelegate
@synthesize stackRef;

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    LuaBridge *brdg = [LuaBridge instance];
    lua_State *L = [brdg L];
    luaopen_cg(L);
    NSString *path = [[NSBundle mainBundle] pathForResource:@"bootstrap" ofType:@"lua"];
    if (!path) {
        NSLog(@"Lua Error: file not found");
    }
    int result = luaL_loadfile(L, [path UTF8String]);
    
    if (result != LUA_OK) {
        NSLog(@"Lua Error: load error: %d: %s", result, lua_tostring(L, -1));
    } else {
        int result = lua_pcall(L, 0, 0, 0);
        if (result) {
            NSLog(@"Lua Error: %s", lua_tostring(L, -1));
        }
    }
    
    lua_getglobal(L, "init");
    [brdg pushObject:self];
    
    if (lua_pcall(L, 1, 1, 0)) {
        NSLog(@"Lua Error: %s", lua_tostring(L, -1));
    } else {
        stackRef = luaL_ref(L, LUA_REGISTRYINDEX);
    }

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
