//
//  main.m
//  TapGame
//
//  Created by Toru Hisai on 13/01/14.
//  Copyright (c) 2013年 Kronecker's Delta Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KDAppDelegate class]));
    }
}
